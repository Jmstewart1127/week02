/**
 * Created by S1375551 on 1/30/2017.
 */
import java.util.Scanner;

/**
 * Created by S1375551 on 1/30/2017.
 */
public class Main5
{

    public static double convertFahrenheitToCelsius(double fahrenheitTemperature)
    {
        return (fahrenheitTemperature - 32.0) * 5.0 / 9.0;
    }

    public static double convertCelsiusToFahrenheit(double celsiusTemperature)
    {
        double fahrenheit = 0.0;

        fahrenheit = celsiusTemperature * 9.0 / 5.0 + 32.0;

        return fahrenheit;
    }


    public static int convertCelsiusToFahrenheit(int celsiusTemperature)
    {
        int fahrenheit = 0;

        fahrenheit = celsiusTemperature * 9 / 5 + 32;

        return fahrenheit;
    }

    public static void main(String[] args)
    {
        // declare and initialize all variables

        Scanner scannerIn = new Scanner(System.in);
        double fahrenheit = 0.0;
        double celsius = 0.0;
        double kelvin = 0.0;
        int count = 0;
        double temperatureSum = 0.0;
        double temperatureAverage = 0.0;
        String keepGoing = "n";



        // get input

        do
        {
            System.out.print("Enter a fahrenheit temperature: ");
            fahrenheit = scannerIn.nextDouble();

            celsius = convertFahrenheitToCelsius(fahrenheit);
            temperatureSum += celsius;
            count++;

            System.out.print("Keep going (Y/N)? ");
            keepGoing = scannerIn.next();

        } while ( keepGoing.toUpperCase().equals("Y") );

        //} while (keepGoing.equals("y") || keepGoing.equals("Y"));

        //while (keepGoing == "y");  // == does not work for strings
        temperatureAverage = temperatureSum / (double)count;



        // do the math


        //display results
        System.out.println("average celsius temp is: " + temperatureAverage);




    }

}
